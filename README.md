# My Salary App

A simple Windows console salary app written in C#.

## About the design decisions

I constructed this little Windows console app trying to use a simple model that uses classes and class inheritance that builds functionality into successive layers of derived classes. 

The app is constructed as it is because I wanted it to remain simple, yet be easy to read, maintain and extend without creating unnecessary complexity.

## How it works

The app works like this:

The user input tests up front for correctly formatted data so that errors are less likely to percolate through the code.

The Salary class collects the input data and calls the Calculator class to do the heavy lifting work of calculating the various outputs.

The Salary class calls the Calculate() method in the topmost derived Calculator class and calculations bubble down through its base classes to calculate figures and then bubble back up to perform the calculation for each derived class.

Levy and Income Tax calculations are all handled by a single method accepting the required inputs for each different type of calculation.

Having a separate derived Calculator class makes it easy to modify any single set of calculations without affecting the others. 

## What if calculations need to change?

If superannuation or tax brackets change, they can easily be modified in one place - only the relevant derived class. Any such changes will have no impact on surrounding classes or code.

Adding functionality for past or future years would involve adding functionality to the Salary class to hold year data and perhaps new derived classes based off the Calculator class to store and perform different calculations for each year - for example the previous year and the upcoming year.

Adding a configuration file and Configuration class and code to apply configuration values to the Calculator classes would allow the user to easily modify tax or levy brackets or superannuation percentage rates as needed.

## What else could I have done?

Add Event and Event Handlers to the Salary and Calculator classes to respond to changes in the salary and pay period entered and recalculate on the fly.

Add Unit Tests to check data inputs and outputs remain as expected as code changes are made.