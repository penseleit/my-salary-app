using System;

namespace MySalary
{
    public class Salary
    {
        private decimal _annualSalary;
        private MySalary.PayPeriod _payPeriod;
        private string _salaryBreakdown;
        private IncomeCalculator _incomeCalculator;


        public Salary()
        {
            _annualSalary = 0;
            _payPeriod = PayPeriod.Week;
            _salaryBreakdown = string.Empty;
            _incomeCalculator = new IncomeCalculator(_annualSalary);


        }

        public decimal AnnualSalary
        {
            get => _annualSalary;
            set
            {
                _annualSalary = value;
                _incomeCalculator.UpdateSalary(_annualSalary);
            }
        }

        public MySalary.PayPeriod PayPeriod
        {
            get => _payPeriod;
            set
            { 
                _payPeriod = value;
                _incomeCalculator.UpdatePayPeriod(_payPeriod);
            }
        }

        public void Reset()
        {
            _annualSalary = 0;
            _payPeriod = PayPeriod.Week;
            _salaryBreakdown = string.Empty;

        }

        public string GetSalaryBreakdown()
        {
            _incomeCalculator.Calculate();
            _salaryBreakdown = _incomeCalculator.SalaryInfo();

            return _salaryBreakdown;
        }
    }
}
