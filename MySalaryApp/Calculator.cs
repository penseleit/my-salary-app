using System;
using System.Globalization;

namespace MySalary
{
    interface ICalculator
    {
        void Calculate();
        string SalaryInfo();
    }

    public class Calculator : ICalculator
    {
        protected decimal _annualPackage;
        protected CultureInfo _auCulture = new CultureInfo("en-AU");


        public Calculator(decimal package)
        {
            _annualPackage = package;
        }

        public virtual void Calculate() { }

        public virtual string SalaryInfo() 
        {
            string message = Environment.NewLine + Environment.NewLine
                + "Gross annual salary package: "
                + _annualPackage.ToString("C", _auCulture)
                + Environment.NewLine + Environment.NewLine;

            return message;
        }
    }

    public class SuperannuationCalculator : Calculator, ICalculator
    {
        private decimal _superannuationPercentageRate;
        protected decimal _annualTaxableIncome;
        protected decimal _annualSuperannuationContribution;

        public SuperannuationCalculator(decimal package) : base(package)
        { 
            _superannuationPercentageRate = 9.5m;
            _annualTaxableIncome = 0m;
            _annualSuperannuationContribution = 0m;
        }

        public override void Calculate()
        {
            _annualTaxableIncome = (base._annualPackage / (100m + _superannuationPercentageRate)) * 100;
            _annualSuperannuationContribution = Decimal.Round(base._annualPackage - _annualTaxableIncome, 2, MidpointRounding.AwayFromZero);
        }

        public override string SalaryInfo()
        {
            string message = base.SalaryInfo()
                + "Annual superannuation contribution: "
                + _annualSuperannuationContribution.ToString("C", base._auCulture)
                + Environment.NewLine + Environment.NewLine
                + "Annual taxable income: "
                + _annualTaxableIncome.ToString("C", base._auCulture)
                + Environment.NewLine + Environment.NewLine;

            return message;
        }
    }

    public class DeductionsCalculator : SuperannuationCalculator, ICalculator
    {
        protected decimal _annualMedicareLevy;
        protected decimal _annualBudgetRepairLevy;
        protected decimal _annualIncomeTax;


        public DeductionsCalculator(decimal package) : base(package)
        { 
            _annualMedicareLevy = 0m;
            _annualBudgetRepairLevy = 0m;
            _annualIncomeTax = 0m;
        }

        public override void Calculate()
        {
            base.Calculate();

            //Calculate Medicare Levy
            decimal roundedDownTaxableIncome = Decimal.Floor(base._annualTaxableIncome);
       

            //Calculate Medicare Levy
            Bracket[] mediLevyBrackets = {
                new Bracket(0m, 0m, 21335m, 0m),
                new Bracket(0m, 21336m, 26668m, 10m),
                new Bracket(0m, 26669m, Decimal.MaxValue, 2m)
                };

            _annualMedicareLevy = GetDeduction(mediLevyBrackets, roundedDownTaxableIncome, false);

            //Calculate Budget Repair Levy
            Bracket[] budLevyBrackets = {
                new Bracket(0m, 0m, 180000m, 0m),
                new Bracket(0m, 180001m, Decimal.MaxValue, 2m)
                };

            _annualBudgetRepairLevy = GetDeduction(budLevyBrackets, roundedDownTaxableIncome, true);


            //Calculate Income Tax
            Bracket[] taxBrackets = {
                new Bracket(0m, 0m, 18200m, 0m),
                new Bracket(0m, 18201m, 37000m, 19m),
                new Bracket(3572m, 37001m, 87000m, 32.5m),
                new Bracket(19822m, 87001m, 180000m, 37m),
                new Bracket(54232m, 180001m, Decimal.MaxValue, 47m)
                };

            _annualIncomeTax = GetDeduction(taxBrackets, roundedDownTaxableIncome, true);

        }

        private decimal GetDeduction(Bracket[] brackets, decimal taxableIncome, bool alwaysDeductExcess)
        {
            decimal deduction = 0;

            foreach (Bracket bracket in brackets)
            {
                if (taxableIncome <= bracket.maxValue)
                {
                    if (bracket.percentageValue > 0m)
                    {
                        decimal deductedValue = bracket.thresholdValue - 1;

                        if (! alwaysDeductExcess)
                        {
                            if (bracket.maxValue == Decimal.MaxValue) //If maxValue is not defined then don't reduce Taxable Income
                            {
                                deductedValue = 0m;
                            }
                        }

                        deduction = bracket.setValue + ((taxableIncome - deductedValue) * bracket.percentageValue / 100);
                        deduction = Decimal.Ceiling(deduction);
                    }
                    break;
                }
            }

            return deduction;
        }

        public override string SalaryInfo() 
        {

            string message = base.SalaryInfo()
                + "Medicare Levy: "
                + _annualMedicareLevy.ToString("C", base._auCulture)
                + Environment.NewLine + Environment.NewLine
                + "Budget Repair Levy: "
                +  _annualBudgetRepairLevy.ToString("C", base._auCulture)
                + Environment.NewLine + Environment.NewLine
                + "Income Tax: "
                + _annualIncomeTax.ToString("C", base._auCulture)
                + Environment.NewLine + Environment.NewLine;

             return message;

        }
    }

    public class IncomeCalculator : DeductionsCalculator, ICalculator
    { 
        protected decimal _annualNetIncome;
        protected decimal _periodPayPacket;
        private PayPeriod _payPeriod;

        public IncomeCalculator(decimal package) : base(package)
        {
            _annualNetIncome = 0m;
            _periodPayPacket = 0m;
            _payPeriod = PayPeriod.Week; //Default
        }

        public override void Calculate()
        {
            base.Calculate();

            _annualNetIncome = base._annualPackage 
                - base._annualSuperannuationContribution 
                - base._annualMedicareLevy
                - base._annualBudgetRepairLevy 
               - base._annualIncomeTax; 

            switch (_payPeriod)
            {
                case PayPeriod.Week:
                    {
                        _periodPayPacket = _annualNetIncome / 52;
                        break;
                    }
                case PayPeriod.Fornight:
                    {
                        _periodPayPacket = _annualNetIncome / 26;
                        break;
                    }
                case PayPeriod.Month:
                    {
                        _periodPayPacket = _annualNetIncome / 12;
                        break;
                    }
                default:
                    {
                        _periodPayPacket = _annualNetIncome / 52;
                        break;
                    }


            }

            Decimal.Round(_periodPayPacket, 2, MidpointRounding.AwayFromZero);

        }

        public override string SalaryInfo()
        {

            string message = base.SalaryInfo()
            + "Annual Net Income: "
             + _annualNetIncome.ToString("C", base._auCulture)
             + Environment.NewLine + Environment.NewLine
             + "Net pay per " + _payPeriod.ToString() + ": "
             + _periodPayPacket.ToString("C", base._auCulture)
             + Environment.NewLine + Environment.NewLine;

            return message;
        }

        public void UpdatePayPeriod(PayPeriod newPeriod)
        {
            _payPeriod = newPeriod;
        }

        public void UpdateSalary(decimal salary)
        {
            base._annualPackage = salary;
        }
    }
}
