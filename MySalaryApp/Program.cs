using System;
using System.Globalization;

namespace MySalary
{
    public enum PayPeriod { Week = 1, Fornight, Month };

    public struct Bracket //Holds levy and tax bracket details
    {
        public decimal setValue;
        public decimal thresholdValue;
        public decimal maxValue;
        public decimal percentageValue;

        public Bracket(decimal setVal, decimal min, decimal max, decimal percentage)
        {
            setValue = setVal;
            thresholdValue = min;
            maxValue = max;
            percentageValue = percentage;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            NumberStyles nStyle = NumberStyles.Currency;
            CultureInfo culture = new CultureInfo("en-AU");
            ConsoleKeyInfo quitKey;
            string salary;
            decimal decSalary;

            //Create Salary Object
            Salary userSalary = new Salary();


            //Welcome message
            string welcome = "-- Welcome to My Salary -- "
            + Environment.NewLine + Environment.NewLine
            + "This application will use your annual salary "
            + Environment.NewLine
            + "to calculate how much you will be paid in each pay period."
            + Environment.NewLine + Environment.NewLine
            + "Enter your annual salary package amount in Australian dollars and cents. For example: $56000.54: > "
            + Environment.NewLine;

            do
            {
                userSalary.Reset();

                Console.Clear();
                Console.Write(welcome);

                //Get salary as string and convert to decimal value
                do
                {
                    salary = string.Empty;
                    while (salary == string.Empty)
                    {
                        salary = Console.ReadLine();
                    }

                    salary = salary.Trim();

                    decSalary = 0;
                    try
                    {
                        decSalary = Decimal.Parse(salary, nStyle, culture);
                        userSalary.AnnualSalary = decSalary;

                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("Invalid value entered. Error: {0}.", e.Message);
                        Console.WriteLine("Please re-enter your annual salary package amount in Australian dollars and cents. > " 
                            + Environment.NewLine + Environment.NewLine);

                    }
                    catch(Exception e)
                    {
                        Console.WriteLine("Invalid value entered. {0}", e.Message);
                        Console.WriteLine("Please re-enter your annual salary package amount in Australian dollars and cents. > "
                            + Environment.NewLine + Environment.NewLine);
                    }

                } while (userSalary.AnnualSalary == 0);

             
                //Get pay period
                ConsoleKeyInfo period;

                do
                {
                    Console.WriteLine("Enter your pay period: Weekly (w), Fortnightly (f) or Monthly (m): > ");

                    period = Console.ReadKey(true);

                } while (period.Key != ConsoleKey.W && period.Key != ConsoleKey.F && period.Key != ConsoleKey.M);

                switch (period.Key)
                {
                    case ConsoleKey.W:
                        userSalary.PayPeriod = PayPeriod.Week;
                        break;
                    case ConsoleKey.F:
                        userSalary.PayPeriod = PayPeriod.Fornight;
                        break;
                    case ConsoleKey.M:
                        userSalary.PayPeriod = PayPeriod.Month;
                        break;
                    default:
                        userSalary.PayPeriod = PayPeriod.Week; //default to week
                        break;
                }

                //Pretend to calculate salary breakdown
                Console.WriteLine(Environment.NewLine 
                    + "Calculating the pay for your pay period...");

                Console.WriteLine(userSalary.GetSalaryBreakdown());

                //Do it again
                Console.WriteLine("Press the Escape key to quit or any other key to enter another annual salary package amount.");

                quitKey = Console.ReadKey();

            } while (quitKey.Key != ConsoleKey.Escape);
     

        }
    }
}
